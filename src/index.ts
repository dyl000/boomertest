import $ from "jquery";
import data from "./data";

console.log(`Data version: ${data.version}`);

interface question {
  question: string;
  boomer: number;
  zoomer: number;
  doomer: number;
  bloomer: number;
}

enum page {
  main = "main",
  questions = "questions",
  results = "results"
}

var questions: question[] = Array();
var answers: string[] = Array();
var result: string[] = Array();
var currentQuestion = 0;

const setPage = (pageName: string) =>
  Object.keys(page).forEach(loopPage => {
    let p = $(`#${loopPage}`);
    loopPage == pageName ? p.show() : p.hide();
  });

const setQuestion = () => {
  if (currentQuestion == questions.length) {
    setPage(page.results);
    return;
  }

  currentQuestion <= 0 ? $("#prevQuestion").hide() : $("#prevQuestion").show();
  $("#questionNumber").html(`${currentQuestion + 1} / ${questions.length}`);
  $("#questionContents").html(questions[currentQuestion].question);
};

$(document).ready(() => {
  setPage(page.main);

  // Load in questions.

  for (let key in data.questions) {
    let qData = data.questions[key];
    questions.push({
      question: key,
      boomer: qData[0],
      zoomer: qData[1],
      doomer: qData[2],
      bloomer: qData[3]
    });
  }

  // Register button callbacks.

  $("#startButton").click(() => {
    setPage(page.questions);
    setQuestion();
  });

  $("#prevQuestion").click(() => {
    currentQuestion -= 1;
    setQuestion();
  });

  $("#nextQuestion").click(() => {
    currentQuestion += 1;
    setQuestion();
  });
});
